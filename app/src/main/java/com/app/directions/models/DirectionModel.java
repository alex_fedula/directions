package com.app.directions.models;

import com.app.directions.database.DirectionDao;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = DirectionDao.TABLE_NAME)
public class DirectionModel {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(columnName = DirectionDao.TABLE_COLUMN_FROM_DIRECTION, dataType = DataType.STRING)
    private String fromDirection;
    @DatabaseField(columnName = DirectionDao.TABLE_COLUMN_TO_DIRECTION, dataType = DataType.STRING)
    private String toDirection;
    @DatabaseField(columnName = DirectionDao.TABLE_COLUMN_TIMESTAMP, dataType = DataType.LONG)
    private long timeStamp;
    @DatabaseField(columnName = DirectionDao.TABLE_COLUMN_POPULARITY, dataType = DataType.INTEGER)
    private int clicked;

    public String getFromDirection() {
        return fromDirection;
    }

    public void setFromDirection(String fromDirection) {
        this.fromDirection = fromDirection;
    }

    public String getToDirection() {
        return toDirection;
    }

    public void setToDirection(String toDirection) {
        this.toDirection = toDirection;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getClicked() {
        return clicked;
    }

    public void setClicked(int clicked) {
        this.clicked = clicked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
