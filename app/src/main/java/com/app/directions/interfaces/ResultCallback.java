package com.app.directions.interfaces;

import java.util.List;

public interface ResultCallback<T> {

    void onResult(List<T> results);
}
