package com.app.directions.utils;


public class Constants {
    public static final int APP_DELAYS = 2000;
    public static final String BROADCAST_FILTER = "local_filter";
    public static final String SYNC_CODE_INTENT_KEY = "sync_key";
    public static final String MESSAGE_INTENT_KEY = "message_key";
    public static final int FINISH_SYNC_CODE = 100;
    public static final int START_SYNC_CODE = 200;
}
