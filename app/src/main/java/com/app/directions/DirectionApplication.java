package com.app.directions;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;

import com.app.directions.database.HelperFactory;
import com.app.directions.network.NetworkStateReceiver;
import com.app.directions.utils.Constants;

public class DirectionApplication extends Application implements NetworkStateReceiver.NetworkStateListener {

    private static DirectionApplication applicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = this;
        HelperFactory.setHelper(this);
        NetworkStateReceiver.setListener(this);
    }


    @Override
    public void onChangeNetworkState(boolean isConnect) {
        if (isConnect) {
            dialogBehavior(null);
        }
    }

    public static void dialogBehavior(final String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                sendBroadcastToListeners(true, getContext(), message);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sendBroadcastToListeners(false, getContext(), null);
                    }
                }, Constants.APP_DELAYS);
            }
        });
    }

    public static void sendBroadcastToListeners(boolean isStartSync, Context context, String message) {
        Intent intent = new Intent(Constants.BROADCAST_FILTER);
        intent.putExtra(Constants.SYNC_CODE_INTENT_KEY,
                isStartSync ? Constants.START_SYNC_CODE : Constants.FINISH_SYNC_CODE);
        intent.putExtra(Constants.MESSAGE_INTENT_KEY, message);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

    }

    public static DirectionApplication getContext() {
        return applicationContext;
    }

}
