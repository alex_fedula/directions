package com.app.directions.network;


import android.os.Handler;
import android.os.Looper;

public abstract class UniversalServerCallBack<T> {
    private final Class<T> typeParameterClass;
    private volatile byte threadMode = MAIN_THREAD_MODE;
    public static final byte BACKGROUND_MODE = -1;
    public static final byte MAIN_THREAD_MODE = 1;

    public byte getThreadMode() {
        return threadMode;
    }

    public UniversalServerCallBack(Class<T> typeParameterClass) {
        this.typeParameterClass = typeParameterClass;
    }

    public UniversalServerCallBack(Class<T> typeParameterClass, byte threadMode) {
        this.typeParameterClass = typeParameterClass;
        this.threadMode = threadMode;
    }



    private void throwFailure(final String message) {
        if (threadMode == MAIN_THREAD_MODE) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    onError(message);
                }
            });
        } else {
            onError(message);
        }
    }


    public void parseResponse(String response) {

    }

    public abstract void onReceive(T model);

    public abstract void onError(String errorMessage);
}
