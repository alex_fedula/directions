package com.app.directions.network;

public class ServerHelper {

    public static final String BASE_URL = "testdomain";
    public static final String SEPARATOR = "/";
    public static final String PARAMETER_SIGN = "?";
    public static final String DELETE_API = "delete";
    public static final String CREATE_API = "create";
    public static final String COLON_SIGN = ":";
    public static final String PARAM1_KEY = "param1";
    public static final String PARAM2_KEY = "param2";

    public static void deleteRecord(int itemId, UniversalServerCallBack<String> callBack) {
        String fullUrl = BASE_URL + SEPARATOR + DELETE_API + SEPARATOR + itemId;
        new Thread(new SimpleWebServerClient(fullUrl, SimpleWebServerClient.GET, null, callBack)).start();
    }
    public static void createRecord(int itemId,String address, UniversalServerCallBack<String> callBack) {
        String parameters = PARAMETER_SIGN + PARAM1_KEY + COLON_SIGN +
                itemId+","+PARAM2_KEY+COLON_SIGN+address;
        String fullUrl = BASE_URL + SEPARATOR + CREATE_API + parameters;
        new Thread(new SimpleWebServerClient(fullUrl, SimpleWebServerClient.POST, null, callBack)).start();
    }

}
