package com.app.directions.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkStateReceiver extends BroadcastReceiver {

    public interface NetworkStateListener {
        void onChangeNetworkState(boolean isConnect);
    }

    private static NetworkStateListener listener = null;

    @Override
    public void onReceive(Context context, Intent intent) {

        final Bundle extras = intent.getExtras();
        if (extras != null) {
            ConnectivityManager connMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = connMan.getActiveNetworkInfo();
            if (ni != null && ni.getState() == NetworkInfo.State.CONNECTED) {

                if (listener != null) {
                    new Thread(
                            new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        HttpURLConnection urlc = (HttpURLConnection)
                                                (new URL(SimpleWebServerClient.TEST_CONNECTION_URL).openConnection());
                                        urlc.setRequestProperty("User-Agent", "Test");
                                        urlc.setRequestProperty("Connection", "close");
                                        urlc.setConnectTimeout(SimpleWebServerClient.CONNECTION_TIMEOUT);
                                        urlc.setReadTimeout(SimpleWebServerClient.CONNECTION_TIMEOUT);
                                        urlc.connect();
                                        if (listener != null)
                                            listener.onChangeNetworkState(urlc.getResponseCode() == 200);
                                    } catch (IOException e) {
                                        Log.e(NetworkStateListener.class.getSimpleName(), "Error checking internet connection", e);
                                        if (listener != null)
                                            listener.onChangeNetworkState(false);
                                    }
                                }
                            }
                    ).start();
                }
            }

            if (intent.getExtras().getBoolean(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                if (listener != null) {
                    listener.onChangeNetworkState(false);
                }
            }
        }
    }

    public static void setListener(NetworkStateListener listener) {
        NetworkStateReceiver.listener = listener;
    }
}