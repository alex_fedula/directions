package com.app.directions.network;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;

public class SimpleWebServerClient implements Runnable {

    String url;
    String verb;
    String content;
    private UniversalServerCallBack<?> universalServerCallBack;
    public static final int CONNECTION_TIMEOUT = 30000;
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String TEST_CONNECTION_URL = "http://www.google.com";

    public SimpleWebServerClient(String url, String verb, String content) {
        this.url = url;
        this.verb = verb;
        this.content = content;
    }

    public SimpleWebServerClient(String url, String verb, String content, UniversalServerCallBack<?> universalServerCallBack) {
        this.url = url;
        this.verb = verb;
        this.content = content;
        this.universalServerCallBack = universalServerCallBack;
    }

    // web service calls
    public String getResponseBody(HttpResponse response) throws Exception {
        String response_text = null;
        HttpEntity entity = null;
        try {
            entity = response.getEntity();
            response_text = _getResponseBody(entity);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            if (entity != null) {
                try {
                    entity.consumeContent();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return response_text;
    }

    public String _getResponseBody(final HttpEntity entity) throws IOException, ParseException {

        if (entity == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        }
        InputStream inputStream = entity.getContent();
        if (inputStream == null) {
            return "";
        }
        if (entity.getContentLength() > Integer.MAX_VALUE) {
            throw new IllegalArgumentException(
                    "HTTP entity too large to be buffered in memory");
        }
        String charset = getContentCharSet(entity);
        if (charset == null) {
            charset = HTTP.DEFAULT_CONTENT_CHARSET;
        }
        Reader reader = new InputStreamReader(inputStream, charset);
        StringBuilder buffer = new StringBuilder();

        try {
            char[] tmp = new char[1024];
            int l;
            while ((l = reader.read(tmp)) != -1) {

                buffer.append(tmp, 0, l);
            }

        } finally {
            reader.close();
        }

        return buffer.toString();
    }

    public String getContentCharSet(final HttpEntity entity) throws ParseException {

        if (entity == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        }
        String charset = null;
        if (entity.getContentType() != null) {
            HeaderElement values[] = entity.getContentType().getElements();
            if (values.length > 0) {
                NameValuePair param = values[0].getParameterByName("charset");
                if (param != null) {
                    charset = param.getValue();
                }
            }
        }
        return charset;
    }


    public void run() {
        HttpResponse response;
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpParams, CONNECTION_TIMEOUT);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
            HttpPost request = new HttpPost(url);
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            if (verb.equals(POST)) {
                StringEntity entity = new StringEntity(content);
                request.setEntity(entity);
            }
            String _response = null;
            response = httpClient.execute(request);
            // int value = response.getStatusLine().getStatusCode();
            _response = this.getResponseBody(response);
            if (universalServerCallBack != null) {
                if (TextUtils.isEmpty(_response)) {
                    throwCallbackError("Server response is empty");
                } else {
                    universalServerCallBack.parseResponse(_response);
                }
            }
        } catch (Exception e) {
            Log.e(SimpleWebServerClient.class.getSimpleName(), "run", e);
            String message = e.getMessage();
            throwCallbackError(message);
        }
    }

    public void throwCallbackError(final String message) {
        if (universalServerCallBack != null) {
            if (universalServerCallBack.getThreadMode() == UniversalServerCallBack.MAIN_THREAD_MODE) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        universalServerCallBack.onError(message);
                    }
                });
            } else {
                universalServerCallBack.onError(message);
            }
        }
    }

}