package com.app.directions.ui;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.app.directions.DirectionApplication;
import com.app.directions.R;
import com.app.directions.adapter.DataBaseResultsAdapter;
import com.app.directions.database.DirectionDao;
import com.app.directions.database.HelperFactory;
import com.app.directions.interfaces.ResultCallback;
import com.app.directions.models.DirectionModel;
import com.app.directions.network.ServerHelper;
import com.app.directions.network.SimpleWebServerClient;
import com.app.directions.utils.Constants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private Button buttonDriving;
    private Button buttonWalking;
    private Button buttonCycling;
    private Button buttonPopularSort;
    private Button buttonRecentSort;
    private Button buttonAlphaSort;
    private DrawerLayout drawerLayout;
    private View contentRoot;
    private View leftDrawer;
    private Toolbar toolbar;
    private ListView listViewContent;
    private EditText editTextFromDirection;
    private EditText editTextToDirection;
    private ProgressDialog progressDialog;

    private List<DirectionModel> listQuery = new ArrayList<>();
    private DataBaseResultsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        configureToolBar();
        configureNavDrawer();
        configureSideBarViews();
        configureMainViews();
        configureSideBarViews();
        adapter = new DataBaseResultsAdapter(listQuery, this);
        View headerView = getLayoutInflater().inflate(R.layout.list_header, listViewContent, false);
        listViewContent.addHeaderView(headerView);
        listViewContent.setAdapter(adapter);
        listViewContent.setOnItemClickListener(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(Constants.BROADCAST_FILTER));
    }

    private void configureMainViews() {
        buttonDriving = (Button) findViewById(R.id.buttonDriving);
        buttonWalking = (Button) findViewById(R.id.buttonWalking);
        buttonCycling = (Button) findViewById(R.id.buttonCycling);
        buttonCycling.setOnClickListener(clickListenerMainContentButtons);
        buttonWalking.setOnClickListener(clickListenerMainContentButtons);
        buttonDriving.setOnClickListener(clickListenerMainContentButtons);
        setLeftButtonState(false, buttonDriving);
        setCentralButtonState(false, buttonWalking);
        setRightButtonState(false, buttonCycling);
        editTextFromDirection = (EditText) findViewById(R.id.editTextFromDirection);
        editTextToDirection = (EditText) findViewById(R.id.editTextToDirection);
        findViewById(R.id.buttonAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toDirection = editTextToDirection.getText().toString().trim();
                String fromDirection = editTextFromDirection.getText().toString().trim();
                if (!TextUtils.isEmpty(toDirection) && !TextUtils.isEmpty(fromDirection)) {
                    DirectionModel directionModel = new DirectionModel();
                    directionModel.setFromDirection(fromDirection);
                    directionModel.setToDirection(toDirection);
                    directionModel.setTimeStamp(Calendar.getInstance().getTimeInMillis());
                    HelperFactory.getHelper().getDirectionDao().create(directionModel);
                    ServerHelper.createRecord(directionModel.getId(), directionModel.getFromDirection(), null);
                    String parameters = ServerHelper.PARAM1_KEY + ServerHelper.COLON_SIGN +
                            directionModel.getId() + "," + ServerHelper.PARAM2_KEY +
                            ServerHelper.COLON_SIGN + directionModel.getFromDirection();
                    String message = SimpleWebServerClient.POST + " url " +
                            ServerHelper.BASE_URL + ServerHelper.SEPARATOR + ServerHelper.CREATE_API +
                            " parameters " + parameters;
                    DirectionApplication.dialogBehavior(message);
                } else {
                    Toast.makeText(MainActivity.this, getString(R.string.fill_fields), Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    private void configureToolBar() {
        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setTitleTextColor(Color.BLACK);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_nav_icon));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleDrawer();
            }
        });

    }

    public void toggleDrawer() {
        if (drawerLayout.isDrawerOpen(leftDrawer)) {
            drawerLayout.closeDrawer(leftDrawer);
        } else {
            drawerLayout.openDrawer(leftDrawer);
        }
    }

    private void configureNavDrawer() {
        contentRoot = findViewById(R.id.contentRoot);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        leftDrawer = findViewById(R.id.left_drawer);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.empty, R.string.empty) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                contentRoot.setTranslationX(slideOffset * drawerView.getWidth());
                drawerLayout.bringChildToFront(drawerView);
                drawerLayout.requestLayout();
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
    }

    private void setLeftButtonState(boolean isPressed, Button button) {
        if (isPressed) {
            button.setBackgroundResource(R.drawable.left_button_pressed_bg);
        } else {
            button.setBackgroundResource(R.drawable.left_button_un_pressed_bg);
        }
    }

    private void setRightButtonState(boolean isPressed, Button button) {
        if (isPressed) {
            button.setBackgroundResource(R.drawable.right_button_pressed_bg);
        } else {
            button.setBackgroundResource(R.drawable.right_button_un_pressed_bg);
        }
    }

    private void setCentralButtonState(boolean isPressed, Button button) {
        if (isPressed) {
            button.setBackgroundResource(R.drawable.central_button_pressed_bg);
        } else {
            button.setBackgroundResource(R.drawable.central_button_un_pressed_bg);
        }
    }

    private View.OnClickListener clickListenerMainContentButtons = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.buttonDriving:
                    setLeftButtonState(true, buttonDriving);
                    setCentralButtonState(false, buttonWalking);
                    setRightButtonState(false, buttonCycling);
                    break;
                case R.id.buttonWalking:
                    setLeftButtonState(false, buttonDriving);
                    setCentralButtonState(true, buttonWalking);
                    setRightButtonState(false, buttonCycling);
                    break;
                case R.id.buttonCycling:
                    setLeftButtonState(false, buttonDriving);
                    setCentralButtonState(false, buttonWalking);
                    setRightButtonState(true, buttonCycling);
                    break;
            }
        }
    };

    private void configureSideBarViews() {
        findViewById(R.id.buttonDeleteAll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HelperFactory.getHelper().getDirectionDao().clearTable();
                listQuery.clear();
                adapter.notifyDataSetChanged();
                DirectionApplication.dialogBehavior(null);
            }
        });

        buttonPopularSort = (Button) findViewById(R.id.buttonPopularSort);
        buttonRecentSort = (Button) findViewById(R.id.buttonRecentSort);
        buttonAlphaSort = (Button) findViewById(R.id.buttonAlphaSort);
        buttonPopularSort.setOnClickListener(clickListenerSideBarButtons);
        buttonRecentSort.setOnClickListener(clickListenerSideBarButtons);
        buttonAlphaSort.setOnClickListener(clickListenerSideBarButtons);
        setLeftButtonState(false, buttonPopularSort);
        setCentralButtonState(false, buttonRecentSort);
        setRightButtonState(false, buttonAlphaSort);
        listViewContent = (ListView) findViewById(R.id.listViewContent);
    }

    private View.OnClickListener clickListenerSideBarButtons = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.buttonPopularSort:
                    setLeftButtonState(true, buttonPopularSort);
                    setCentralButtonState(false, buttonRecentSort);
                    setRightButtonState(false, buttonAlphaSort);
                    HelperFactory.getHelper().getDirectionDao().
                            getAllDirections(DirectionDao.SORT_BY_POPULAR, new ResultCallback<DirectionModel>() {

                                @Override
                                public void onResult(List<DirectionModel> results) {
                                    updateListInAdapterBehavior(results);
                                }
                            });
                    break;
                case R.id.buttonRecentSort:
                    setLeftButtonState(false, buttonPopularSort);
                    setCentralButtonState(true, buttonRecentSort);
                    setRightButtonState(false, buttonAlphaSort);
                    HelperFactory.getHelper().getDirectionDao().
                            getAllDirections(DirectionDao.SORT_BY_RECENTS, new ResultCallback<DirectionModel>() {

                                @Override
                                public void onResult(List<DirectionModel> results) {
                                    updateListInAdapterBehavior(results);
                                }
                            });
                    break;
                case R.id.buttonAlphaSort:
                    setLeftButtonState(false, buttonPopularSort);
                    setCentralButtonState(false, buttonRecentSort);
                    setRightButtonState(true, buttonAlphaSort);
                    HelperFactory.getHelper().getDirectionDao().
                            getAllDirections(DirectionDao.SORT_BY_ALPHABET, new ResultCallback<DirectionModel>() {

                                @Override
                                public void onResult(List<DirectionModel> results) {
                                    updateListInAdapterBehavior(results);
                                }
                            });
                    break;
            }
        }
    };

    private void updateListInAdapterBehavior(List<DirectionModel> results) {
        listQuery.clear();
        listQuery.addAll(results);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra(Constants.SYNC_CODE_INTENT_KEY)) {
                int code = intent.getIntExtra(Constants.SYNC_CODE_INTENT_KEY, Constants.FINISH_SYNC_CODE);
                String message = intent.getStringExtra(Constants.MESSAGE_INTENT_KEY);

                switch (code) {
                    case Constants.FINISH_SYNC_CODE:
                        tryDismissProgressDialog();
                        break;
                    case Constants.START_SYNC_CODE:
                        tryDismissProgressDialog();
                        progressDialog = new ProgressDialog(MainActivity.this);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setMessage(message != null ? message : getString(R.string.sync_bar_text));
                        progressDialog.show();
                        break;
                }
            }
        }
    };

    private void tryDismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //mean that header
        if (position == 0) {
            toggleDrawer();
        } else {
            ///list item click block
            makeDirectionMorePopular(position);
        }

    }

    private void makeDirectionMorePopular(int position) {
        if (position != 0) {
            //correction for header
            position--;
            DirectionModel directionModel = listQuery.get(position);
            directionModel.setClicked(directionModel.getClicked() + 1);
            HelperFactory.getHelper().getDirectionDao().update(directionModel);
        }
    }
}
