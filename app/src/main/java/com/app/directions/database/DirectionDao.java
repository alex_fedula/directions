package com.app.directions.database;

import android.os.AsyncTask;
import android.util.Log;

import com.app.directions.interfaces.ResultCallback;
import com.app.directions.models.DirectionModel;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DirectionDao extends BaseDaoImpl<DirectionModel, Integer> {

    public static final String TABLE_NAME = "directions";
    public static final String TABLE_COLUMN_FROM_DIRECTION = "from_direction";
    public static final String TABLE_COLUMN_TIMESTAMP = "timestamp";
    public static final String TABLE_COLUMN_POPULARITY = "popularity";
    public static final String TABLE_COLUMN_TO_DIRECTION = "to_direction";

    public static final int SORT_BY_POPULAR = 100;
    public static final int SORT_BY_RECENTS = 200;
    public static final int SORT_BY_ALPHABET = 300;

    protected DirectionDao(ConnectionSource connectionSource, Class<DirectionModel> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    @Override
    public List<DirectionModel> queryForAll() throws SQLException {
        return super.queryForAll();
    }

    public void getAllDirections(final int sortType, final ResultCallback<DirectionModel> callback) {
        AsyncTask asyncTask = new AsyncTask<Object, Void, List<DirectionModel>>() {

            @Override
            protected List<DirectionModel> doInBackground(Object... params) {
                QueryBuilder<DirectionModel, Integer> queryBuilder = queryBuilder();
                List<DirectionModel> results = new ArrayList<>();
                try {
                    switch (sortType) {
                        case SORT_BY_POPULAR:
                            queryBuilder.orderBy(TABLE_COLUMN_POPULARITY, false);
                            results.addAll(queryBuilder.query());
                            break;
                        case SORT_BY_RECENTS:
                            queryBuilder.orderBy(TABLE_COLUMN_TIMESTAMP, false);
                            results.addAll(queryBuilder.query());
                            break;
                        case SORT_BY_ALPHABET:
                            queryBuilder.orderBy(TABLE_COLUMN_FROM_DIRECTION, true);
                            results.addAll(queryBuilder.query());
                            break;

                    }
                } catch (Exception e) {

                }
                return results;
            }

            @Override
            protected void onPostExecute(List<DirectionModel> directionModels) {
                if (callback != null) {
                    callback.onResult(directionModels);
                }
            }
        };
        asyncTask.execute();
    }

    @Override
    public int create(final DirectionModel data) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DirectionDao.super.create(data);
                } catch (SQLException e) {
                    Log.e(DirectionDao.class.getSimpleName(), "" + e.getMessage());
                }
            }
        }).start();
        return 1;
    }

    @Override
    public int delete(final DirectionModel data) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DirectionDao.super.delete(data);
                } catch (SQLException e) {
                    Log.e(DirectionDao.class.getSimpleName(), "" + e.getMessage());
                }
            }
        }).start();
        return 1;
    }

    @Override
    public int update(final DirectionModel data) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DirectionDao.super.update(data);
                } catch (SQLException e) {
                    Log.e(DirectionDao.class.getSimpleName(), "" + e.getMessage());
                }
            }
        }).start();
        return 1;
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(getConnectionSource(), DirectionModel.class);
        } catch (SQLException e) {
            Log.e(DirectionDao.class.getSimpleName(), "" + e.getMessage());

        }
    }


}
