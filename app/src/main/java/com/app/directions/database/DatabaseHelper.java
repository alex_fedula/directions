package com.app.directions.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.app.directions.models.DirectionModel;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "directions.db";
    private static final int DATABASE_VERSION = 1;


    private DirectionDao directionDao = null;


    public DirectionDao getDirectionDao() {
        if (directionDao == null) {
            try {
                directionDao = new DirectionDao(getConnectionSource(), DirectionModel.class);
            } catch (SQLException e) {
                Log.e(DatabaseHelper.class.getSimpleName(), ""+e.getMessage());
            }
        }
        return directionDao;
    }


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        createDataBaseTablesIfNotExists();

    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        dropDataBaseTables(database);
    }

    public void createDataBaseTablesIfNotExists(ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, DirectionModel.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getSimpleName(), "" + e.getMessage());
        }
    }

    public void createDataBaseTablesIfNotExists() {
        createDataBaseTablesIfNotExists(connectionSource);
    }

    public void dropDataBaseTables() {
        try {
            TableUtils.clearTable(connectionSource, DirectionDao.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropDataBaseTables(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTS " + DirectionDao.TABLE_NAME);
    }


    @Override
    public void close() {
        super.close();
        directionDao = null;
    }

}
