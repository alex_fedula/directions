package com.app.directions.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.directions.DirectionApplication;
import com.app.directions.R;
import com.app.directions.database.HelperFactory;
import com.app.directions.models.DirectionModel;
import com.app.directions.network.ServerHelper;
import com.app.directions.network.SimpleWebServerClient;
import com.app.directions.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class DataBaseResultsAdapter extends BaseAdapter {

    private List<DirectionModel> listDirections = new ArrayList<>();
    private MainActivity context;
    private LayoutInflater inflater;

    public DataBaseResultsAdapter(List<DirectionModel> listDirections, MainActivity context) {
        this.listDirections = listDirections;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listDirections != null ? listDirections.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        DirectionModel directionModel = listDirections.get(position);
        holder.textViewTitle.setClickable(false);
        holder.textViewTitle.setText("Address from: " + directionModel.getFromDirection() + " to: " + directionModel.getToDirection());
        holder.imageViewIcon.setVisibility(View.VISIBLE);
        holder.imageViewIcon.setText(context.getString(R.string.delete));
        holder.imageViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String message = SimpleWebServerClient.GET + " url " +
                            ServerHelper.BASE_URL + ServerHelper.SEPARATOR + ServerHelper.DELETE_API +
                            ServerHelper.SEPARATOR + listDirections.get(position).getId();
                    HelperFactory.getHelper().getDirectionDao().delete(listDirections.get(position));
                    ServerHelper.deleteRecord(listDirections.get(position).getId(), null);
                    listDirections.remove(position);
                    notifyDataSetChanged();
                    DirectionApplication.dialogBehavior(message);
                } catch (Exception e) {
                    Log.e(DataBaseResultsAdapter.class.getSimpleName(), "" + e.getMessage());
                }
            }
        });
        return convertView;
    }

    private static class Holder {
        private TextView textViewTitle;
        private TextView imageViewIcon;

        public Holder(View view) {
            this.textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
            this.imageViewIcon = (TextView) view.findViewById(R.id.imageViewIcon);
        }
    }
}
